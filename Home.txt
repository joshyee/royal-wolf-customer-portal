{% assign forums_sm = sitemarkers["Forums"] %}


{% if user %}


    <div class="col-md-8">
      <div class="container pt-5 home-box-wrapper">
        <h1>Welcome To Your Portal</h1>
        <div class="row">
          <div class="col-6 col-md-3">
            <a href="/account-detail" class="home-box-link">
              <div class="home-box">
                <img src="/account-information.png" alt="account">
                <div class="home-box-icon account-icon"></div>
                <div class="home-box-txt h3">Account</div>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-3">
            <a href="/" class="home-box-link">
              <div class="home-box">
                <img src="/Contact-Us.jpg" alt="Phone">
                <div class="home-box-icon phone-icon"></div>
                <div class="home-box-txt h3">Company Contacts</div>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-3">
            <a href="/PortalContainersonRentList" class="home-box-link">
              <div class="home-box">
                <img src="/containers-on-hire.png" alt="Containers">
                <div class="home-box-icon container-hire-icon"></div>
                <div class="home-box-txt h3">Containers on hire</div>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-3">
            <a href="/quotes-list" class="home-box-link">
              <div class="home-box">
                <img src="/Quotes.png" alt="Quotes">
                <div class="home-box-icon receipt-icon"></div>
                <div class="home-box-txt h3">Quotes</div>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-3">
            <a href="/AccountStatement" class="home-box-link">
              <div class="home-box">
                <img src="/Statements.png" alt="Statements">
                <div class="home-box-icon statement-icon"></div>
                <div class="home-box-txt h3">Statements</div>
              </div>
            </a>
          </div>


          <div class="col-6 col-md-3">
            <a href="/InvoicesandStatements" class="home-box-link">
              <div class="home-box">
                <img src="/make-a-payment.png" alt="Pay an Invoice">
                <div class="home-box-icon payment-icon"></div>
                <div class="home-box-txt h3">Pay an Invoice</div>
              </div>
          </div>
          <div class="col-6 col-md-3">
            <a href="/InvoicesandStatements" class="home-box-link">
              <div class="home-box">
                <img src="/Invoices.png" alt="Invoices">
                <div class="home-box-icon invoice-icon"></div>
                <div class="home-box-txt h3">Invoices</div>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-3">
            <a href="https://www.royalwolf.com.au/" class="home-box-link">
              <div class="home-box">
                <img src="/back-to-website.jpg" alt="Back To Website">
                <div class="home-box-icon dashboard-icon"></div>
                <div class="home-box-txt h3">Back to website</div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

  </div> <!-- Closing Row -->
</div> <!-- Closing Container -->


<!--
<div class="row">
  <div class="col-md-3 col-xs-12"><br /><br />
  </div>
</div>
-->

</div> <!-- End container -->

{% else %}
<!--<section class="page_section section-landing">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h1 class="section-landing-heading">{% editable snippets 'Home/Title' %}</h1>
                <h2 class="section-landing-sub-heading">{% editable snippets 'Home/Subtitle' %}</h2>

            </div>
        </div>
    </div>
    <div class="layer_down">&nbsp;</div>
</section>-->
<h1> Royal Wolf Customer Portal</h1>

{% endif %}
